from django.urls import path
from .views import *
urlpatterns = [
    path('', index, name = "index"),
    path('list', images_list, name = "image_list"),
    path('add', create_image, name = "image_add"),
    path('category', create_category, name = "category_add"),
    path('edit/<int:id>', create_image, name = "image_edit"),
    path('category/edit/<int:id>', create_category, name = "category_edit"),
]