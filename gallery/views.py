from django.shortcuts import render, get_object_or_404
from .models import *
from .forms import *
from django.shortcuts import redirect


def index(request):
    cat = Category.objects.all()
    return render(request, 'gallery/base.html',{'cat':cat})


def images_list(request):
    cat = request.GET.get('category',None)
    if cat:
        images = Images.objects.filter(category=cat).order_by('-created_at')
    else:
        images = Images.objects.all().order_by('-created_at')
    cat = Category.objects.all()
    context = {}
    form = ImageForm()
    context.update({
        "images":images,
        'cat':cat,
        'form':form
    })
    return  render(request,"gallery/images_list.html", context)


def create_image(request, id = None):
    obj = get_object_or_404(Images, id=id) if id else None
    if request.method == "POST":

        form = ImageForm(request.POST,request.FILES, instance=obj)
        if form.is_valid():
            form.save(Images)
            return redirect('/photo/list')
    else:
        form = ImageForm(instance=obj)
    return render(request, "gallery/add_image.html", {'form':form, 'obj':obj})


def create_category(request, id = None):
    obj = get_object_or_404(Category, id=id) if id else None
    if request.method == "POST":

        form = CategoryForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect('/photo/category')
    else:
        form = CategoryForm(instance=obj)
        cat = Category.objects.all().order_by('-created_at')
    return render(request, "gallery/add_cat.html", {'form':form, 'obj':obj,'cat':cat})


