steps to run project
------------------------
1. craete virtual env
    python3 -m venv env
2. activate the env
    source env/bin/activate
3. cd src 
4. pip install -r requirements.txt 
5. python manage.py migrate
6. python manage.py runserver
